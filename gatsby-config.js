module.exports = {
  siteMetadata: {
    title: 'POC gatsby',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-transformer-remark',
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Open Sans\:400,600,700`,
        ]
      }
    },
    'gatsby-plugin-typescript',
    {
      resolve: 'gatsby-source-contentful',
      options: {
        spaceId: '',
        accessToken: ''
      }
    }
  ]
};

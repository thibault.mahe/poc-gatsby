// Updating Webpack
// -------------------------

exports.modifyWebpackConfig = ({ config, stage }) => {
  switch (stage) {
    case "develop":
      config.loader("css", {
        include: /flexboxgrid/,
      });

      break;

    case "build-css":
      config.loader("css", {
        include: /flexboxgrid/,
      });

      break;

    case "build-html":
      config.loader("css", {
        include: /flexboxgrid/,
      });

      break;

    case "build-javascript":
      config.loader("css", {
        include: /flexboxgrid/,
      });

      break;
  }

  return config;
};

// Creating new blog entries
// -------------------------
const path = require('path');

exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators;

  return new Promise((resolve, reject) => {
    resolve(
      graphql(
        `
          {
            allContentfulArticle {
              edges {
                node {
                  id,
                  slug
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          reject(result.errors);
        }

        // Create pages for each article
        result.data.allContentfulArticle.edges.forEach(({ node }) => {
          const article = node;
          createPage({
            path: `/articles/${article.slug}`,
            component: path.resolve(`src/templates/article.tsx`),
            context: {
              id: article.id,
              slug: article.slug
            }
          });
        });
      })
    );
  });
};

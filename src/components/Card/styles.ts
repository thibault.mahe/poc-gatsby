import { style } from 'typestyle';
import { typography, colors } from '../../styles/variables';

namespace styles {
  export const card = style({
    $debugName: 'card',
    display: 'block',
    border: '1px solid red',
    fontSize: '1rem',
    textDecoration: 'none',
  });

  export const cardCover = style({
    $debugName: 'card__cover',
    position: 'relative',
    height: '200px',
    border: '1px solid green',

    $nest: {
      ' img': {
        height: '100%',
        objectFit: 'cover',
        fontFamily: 'object-fit: cover;',
        // Temp
        width: '100%',
      }
    }
  });

  export const cardTitle = style({
    $debugName: 'card__title',
    fontSize: '2em',
    fontWeight: typography.fontWeightBold,
    color: colors.pinkBase,
    lineHeight: '2.4rem',
  });

  export const cardAvatar = style({
    $debugName: 'card__avatar',
    height: '50px',
    width: '50px',
    borderRadius: '50%',
    overflow: 'hidden',
    border: '1px solid red',

    $nest: {
      ' img': {
        height: '100%',
        // Temp
        width: '100%',
      }
    }
  });
}

export default styles;

import * as React from 'react';

import * as objectFitImages from 'object-fit-images';

import { classes } from 'typestyle';

import { Row, Col } from 'react-flexbox-grid';

import { image, headings } from '../../styles';

import styles from './styles';

interface CardProps {
  contentLink: string,
  contentIlluHref: string,
  contentTitle: string,
  contentExcerpt: string,
  contentDate: string,
  authorAvatarUrl: string,
  authorName: string
}

// Activate ovject fit plugin
objectFitImages();

export default ({ contentLink, contentIlluHref, contentTitle, contentExcerpt, contentDate, authorAvatarUrl, authorName }: CardProps) => {

  return (
    <a href={contentLink} className={styles.card}>

      <header className={styles.cardCover}>
        <img
          src={contentIlluHref}
          className={image.default.img}
          alt="" />
      </header>

      <div className="">

        <h2 className={classes(headings.default.h2, styles.cardTitle)}>{contentTitle}</h2>
        <p dangerouslySetInnerHTML={{
          __html: contentExcerpt
        }}/>

        <hr />

        <Row start="xs" middle="xs">
          <Col xs>
            <div className={styles.cardAvatar}>
              <img
                src={authorAvatarUrl}
                className={image.default.img}
                alt="" />
            </div>
          </Col>
          <Col xs>
            <p className="">{authorName}</p>
            <p className="">{contentDate}</p>
          </Col>
        </Row>

        <hr />

      </div>

    </a>
  );
};

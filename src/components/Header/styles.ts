import { style } from 'typestyle';

import { typography, colors } from '../../styles/variables';

namespace styles {
  export const header = style({
    $debugName: 'header',
    backgroundColor: colors.grey10,

    $nest: {
      ' a': {
        color: colors.grey80,
      }
    }
  });

  export const headerContent = style({
    $debugName: 'header__content',
    height: '60px'
  });
}

export default styles;

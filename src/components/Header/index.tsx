import * as React from 'react';
import Link, { withPrefix } from 'gatsby-link';
import { Row, Col } from 'react-flexbox-grid';
import Container from '../Container';

import styles from './styles';

interface HeaderProps {
  children: React.ReactNode;
}

export default ({ children }: HeaderProps) => (
  <header className={styles.header}>
    <Container>
      <Row middle="xs" className={styles.headerContent}>
        <Link to="/">
          Accueil
        </Link>
        {children}
      </Row>
    </Container>
  </header>
);

import { style } from 'typestyle';
import { fonts, colors } from '../../styles';

namespace styles {
  export const button = style({
    $debugName: 'button',
    backgroundColor: 'white',
    // border: `1px solid ${colors.grayDark1}`,
    boxSizing: 'border-box',
    // color: colors.grayDark1,
    cursor: 'pointer',
    // fontSize: fonts.small,
    letterSpacing: '1.5px',
    padding: '12px 15px',
    textAlign: 'center',
    textDecoration: 'none',
    textTransform: 'uppercase',
    transition: 'all .45s',

    $nest: {
      '&:hover': {
       //  backgroundColor: colors.grayDark1,
       //  color: colors.white
      }
    }
  });

  export const buttonPrimary = style({
    $debugName: 'buttonPrimary',
    // backgroundColor: colors.primary,
    border: 'none',
    // color: colors.white,

    $nest: {
      '&:hover': {
        // backgroundColor: colors.primaryDark,
      }
    }
  });

  export const buttonLink = style({
    $debugName: 'buttonLink',
    // color: colors.grayDark1,
    textDecoration: 'none',
    transition: 'all .45s',

    $nest: {
      '&:hover': {
        // color: colors.grayDark3
      }
    }
  });
}

export default styles;

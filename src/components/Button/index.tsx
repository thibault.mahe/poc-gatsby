import * as React from 'react';
import Link from 'gatsby-link';
import { classes } from 'typestyle';

import styles from './styles';

interface ButtonProps {
  children: React.ReactNode;
  type?: 'default' | 'primary' | 'link';
  to?: string;
  href?: string;
  onClick?: Function;
}

export default ({ children, type = 'default', to, href, onClick }: ButtonProps) => {
  const className = classes(
    (type === 'default' || type === 'primary') && styles.button,
    (type === 'primary') && styles.buttonPrimary,
    (type === 'link') && styles.buttonLink
  );

  return (
    to && (
      <Link to={to} className={className}>
        {children}
      </Link>
    )
  ) || (
    href && (
      <a href={href} target="_blank" className={className}>
        {children}
      </a>
    )
  ) || (
    onClick && (
      <button onClick={() => onClick()} className={className}>
        {children}
      </button>
    )
  ) || null;
};

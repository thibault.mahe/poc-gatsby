import { style } from 'typestyle';

namespace styles {
  export const navbar = style({
    $debugName: 'navbar',
    display: 'flex',
    flex: '1',
    height: 'inherit',
    marginLeft: '40px'
  });

  export const navbarItems = style({
    $debugName: 'navbarItems',
    height: 'inherit',
    display: 'flex',
    flex: 1,
    margin: 0,
    padding: 0,
    alignItems: 'center'
  });

  export const navbarItemsRight = style({
    $debugName: 'navbarItemsRight',
    justifyContent: 'flex-end'
  });

  export const navbarItem = style({
    $debugName: 'navbarItem',
    display: 'inline-block',
    fontWeight: 100,
    marginRight: '20px',
    $nest: {
      '&:last-child': {
        marginRight: 0
      }
    }
  });
 
}

export default styles;

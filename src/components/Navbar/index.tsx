import * as React from 'react';

import Button from '../Button';
import NavbarItems from './Items';
import NavbarItem from './Item';
import styles from './styles';

export default () => (
  <nav className={styles.navbar}>

    <NavbarItems>
      <NavbarItem>
        <Button type="link" to="/articles">
          Articles
        </Button>
        <Button type="link" to="/about">
          About
        </Button>
      </NavbarItem>
    </NavbarItems>

  </nav>
);

import * as React from 'react';
import { classes } from 'typestyle';

import Item from './Item';
import styles from './styles';

interface NavbarItemsProps {
  children: React.ReactNode;
  right?: Boolean;
}

const Items = ({ children, right }: NavbarItemsProps) => (
  <ul className={classes(styles.navbarItems, right && styles.navbarItemsRight)}>
    {children}
  </ul>
);

export default Items;

import * as React from 'react';

import styles from './styles';

interface NavbarItemProps {
  children: React.ReactNode;
}

export default ({ children }: NavbarItemProps) => (
  <li className={styles.navbarItem}>
    {children}
  </li>
);

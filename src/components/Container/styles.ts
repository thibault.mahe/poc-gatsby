import { style } from 'typestyle';
import { screens } from '../../styles/variables';

const enum breakpointEnum {
  sm,
  md,
  lg,
  xl
};

const columnPadding = '15px';

namespace styles {
  const containerSizeSM = '540px';
  const containerSizeMD = '720px';
  const containerSizeLG = '960px';
  const containerSizeXL = '1140px';

  const containerBasis = {
    position: 'relative',
    maxWidth: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingLeft: columnPadding,
    paddingRight: columnPadding,
  };

  const makeContainer = (breakpoint: breakpointEnum) => {
    if (breakpoint == breakpointEnum.sm) {
      return screens.mediaSM(
      screens.breakpointMD - 1,
      {
        width: containerSizeSM,
      });
    }
    else if (breakpoint == breakpointEnum.md) {
      return screens.mediaMD(
      screens.breakpointLG - 1,
      {
        width: containerSizeMD,
      });
    }
    else if (breakpoint == breakpointEnum.lg) {
      return screens.mediaLG(
      screens.breakpointXL - 1,
      {
        width: containerSizeLG,
      });
    }
    else if (breakpoint == breakpointEnum.xl) {
      return screens.mediaXL(
      false,
      {
        width: containerSizeXL,
      });
    }

  };

  export const container = style(
    {
      $debugName: 'container',
      ...containerBasis,
    },
    makeContainer(breakpointEnum.sm),
    makeContainer(breakpointEnum.md),
    makeContainer(breakpointEnum.lg),
    makeContainer(breakpointEnum.xl),
  );

  export const containerSM = style(
    {
      $debugName: 'container-sm',
    ...containerBasis,
    },
    makeContainer(breakpointEnum.sm),
  );

  export const containerMD = style(
    {
      $debugName: 'container-md',
    ...containerBasis,
    },
    makeContainer(breakpointEnum.sm),
    makeContainer(breakpointEnum.md),
  );

  export const containerLG = style(
    {
      $debugName: 'container-lg',
    ...containerBasis,
    },
    makeContainer(breakpointEnum.sm),
    makeContainer(breakpointEnum.md),
    makeContainer(breakpointEnum.lg),
  );
}

export default styles;

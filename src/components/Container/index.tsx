import * as React from 'react';
import { classes } from 'typestyle';

import styles from './styles';

interface ContainerProps {
  children: React.ReactNode;
  size?: 'sm' | 'md' | 'xl';
}

export default ({ children, size }: ContainerProps) => {
  const className =
    (size === 'sm') && styles.containerSM ||
    (size === 'md') && styles.containerMD ||
    (size === 'xl') && styles.containerXL ||
    styles.container;

  return (
    <div className={className}>
      {children}
    </div>
  );
};

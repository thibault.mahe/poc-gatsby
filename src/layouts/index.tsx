import * as React from 'react';
import Link from 'gatsby-link';
// import Helmet from 'react-helmet'; // TODO: site metadata

import Container from '../components/Container';
import Header from '../components/Header';
import Navbar from '../components/Navbar';

interface DefaultLayoutProps {
  children: Function;
}

export default ({ children }: DefaultLayoutProps) => (
  <div>

    <Header>
      <Navbar />
    </Header>

    <Container>
      {children()}
    </Container>

  </div>
);

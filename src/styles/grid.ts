import { style } from 'typestyle';
import { screens } from './variables';

namespace styles {

  export const grid = style({
    $debugName: 'grid',
    display: 'flex',
    flexWrap: 'wrap',
  });

  function column(suffix: string) {
    return ({
      $nest: {
        '&-full${suffix}': {
          width: '100%',
        },
        '&-one-half${suffix}': {
          width: '50%',
        },
        '&-one-third${suffix}': {
          width: '33.3333%',
        },
        '&-two-third${suffix}': {
          width: '66.6666%',
        },
        '&-one-quarter${suffix}': {
          width: '25%',
        },
        '&-three-quarter${suffix}': {
          width: '75%',
        },
        '&-one-fifth${suffix}': {
          width: '20%',
        },
        '&-two-fifth${suffix}': {
          width: '40%',
        },
        '&-three-fifth${suffix}': {
          width: '60%',
        },
        '&-four-fifth${suffix}': {
          width: '80%',
        },
      }
    });
  }

  export const col = style(
    {
      $debugName: 'col',
      position: 'relative',
      display: 'block',
      // Prevent columns from collapsing when empty
      minHeight: '1px',
      flexBasis: 0,
      flexGrow: 1,
      flexShrink: 1,
      flexWrap: 'wrap',
    },
    column(),
    screens.mediaMain({
    },
    column('\@main')),
  );

}

export default styles;

import { style, cssRule } from 'typestyle';
import { colors } from './variables';

namespace styles {

  export const img = style(
    {
      $debugName: 'img',
      maxWidth: '100%',
      height: 'auto',
    },
  );

  cssRule('img', {
    lineHeight: 3,
    textAlign: 'center',
    position: 'relative',
    display: 'block',
  });

  cssRule('img:before', {
    content: '"Oh mince, le visuel ne semble pas être disponible :("',
    display: 'block',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    marginBottom: '10px',
    color: colors.grey20,
    background: colors.grey70,
    border: '2px dotted ${colors.grey60}',
  });

}

export default styles;

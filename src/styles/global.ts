import { style, cssRaw, cssRule } from 'typestyle';
import { colors, typography, transition } from './variables';

namespace styles {

  cssRule('html, body', {
    margin: '0',
    fontSize: typography.fontSizeBase,
    lineHeight: typography.lineHeightBase,
    fontFamily: typography.fontFamilyBase,
    color: colors.color10,
  });

  cssRule('a', {
    color: typography.linkTextColor,
    textDecoration: typography.linkTextDecoration,
    transition: 'color ' + transition.transitionDefault,

    $nest: {
      '&:hover, &:focus': {
        color: typography.linkHoverColor,
        textDecoration: typography.linkHoverDecoration,
      }
    }
  });

}

export default styles;

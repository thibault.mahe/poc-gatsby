import * as resetModule from './reset';
import * as globalModule from './global';
import * as headingsModule from './headings';
import * as imageModule from './image';

export const reset = resetModule;
export const global = globalModule;
export const headings = headingsModule;
export const image = imageModule;

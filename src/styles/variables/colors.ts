// ------------
// OPTIONS
// ------------

export const grey10 = '#262729';
export const grey20 = '#313131';
export const grey30 = '#4d4d4d';
export const grey40 = '#888';
export const grey50 = '#aeaeae';
export const grey60 = '#ccc';
export const grey70 = '#eaeaea';
export const grey80 = '#f4f4f4';

export const blueBase = '#2b7de8';
export const blueDeep = '#38bfea';
export const blueDark = '#3239e8';

export const greenBase = '#30d8e3';
export const greenDeep = '#34d8fb';
export const greenDark = '#5ed3a0';

export const orangeBase = 'orange';
export const orangeDeep = '#ffcb7a';
export const orangeDark = '#f245aa';

export const pinkBase = '#E4325F';

// ------------
// DECISIONS
// ------------

export const primaryColor = pinkBase;
export const secondaryColor = greenBase;
export const tertiaryColor = orangeBase;

export const transitionTimeDefault = '0.4s'

export const transitionEasingDefault = 'Linear';

export const transitionDefault = transitionTimeDefault + ' ' + transitionEasingDefault;

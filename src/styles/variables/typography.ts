import { primaryColor } from './colors';

export const fontFamilyBase = '"Open Sans", Helvetica, sans-serif';
export const fontFamilyHeading = fontFamilyBase;

export const lineHeightBase = 1.4;

export const fontSizeBase = '14px';
export const fontSizeXXS = '8px';
export const fontSizeXS = '11px';
export const fontSizeS = '16px';
export const fontSizeM = '20px';
export const fontSizeL = '28px';
export const fontSizeXL = '32px';
export const fontSizeXXL = '40px';
export const fontSizeXXXL = '70px';

export const fontWeightLight = 300;
export const fontWeightRegular = 400;
export const fontWeightMedium = 500;
export const fontWeightSemibold = 600;
export const fontWeightBold = 700;

export const linkTextColor = 'currentColor';
export const linkTextDecoration = 'none';
export const linkHoverColor = primaryColor;
export const linkHoverDecoration = 'none';


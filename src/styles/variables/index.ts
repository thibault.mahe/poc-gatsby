import * as colorsModule from './colors';
import * as typographyModule from './typography';
import * as screensModule from './screens';
import * as transitionModule from './transition';

export const colors = colorsModule;
export const typography = typographyModule;
export const screens = screensModule;
export const transition = transitionModule;

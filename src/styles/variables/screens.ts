import { media, types } from 'typestyle';

export const breakpointXS = 0;
export const breakpointSM = 576;
export const breakpointMD = 768;
export const breakpointLG = 992;
export const breakpointXL = 1200;

export const breakpointMain = breakpointMD;

const mediaScreen = (minSize: string, maxSize: string, objects: types.NestedCSSProperties[]) => {
  if (maxSize) {
    return media(
      {
        type: 'screen',
        minWidth: minSize,
        maxWidth: maxSize
      },
      ...objects
    )
  }
  else {
    return media(
      {
        type: 'screen',
        minWidth: minSize,
      },
      ...objects
    )
  }
};

// Function exports
export const mediaSM = (maxSize: string, ...objects: types.NestedCSSProperties[]) => mediaScreen(breakpointSM, maxSize, objects);
export const mediaMD = (maxSize: string, ...objects: types.NestedCSSProperties[]) => mediaScreen(breakpointMD, maxSize, objects);
export const mediaLG = (maxSize: string, ...objects: types.NestedCSSProperties[]) => mediaScreen(breakpointLG, maxSize, objects);
export const mediaXL = (maxSize: string, ...objects: types.NestedCSSProperties[]) => mediaScreen(breakpointXL, maxSize, objects);

export const mediaMain = (maxSize: string, ...objects: types.NestedCSSProperties[]) => mediaScreen(breakpointMain, maxSize, objects);

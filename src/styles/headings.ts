import { style } from 'typestyle';

import { typography, screens } from './variables';

namespace styles {

  const headingH1SizeMobile = typography.fontSizeXXL;
  const headingH1SizeDesktop = typography.fontSizeXXXL;
  const headingH2SizeMobile = typography.fontSizeL;
  const headingH2SizeDesktop = typography.fontSizeXXL;
  const headingH3SizeDesktop = typography.fontSizeM;
  const headingH4SizeDesktop = typography.fontSizeS;

  const heading = {
    fontFamily: typography.fontFamilyHeading,
    lineHeight: 1,
    margin: 0,
  };

  export const h1 = style(
    {
      ...heading,
      $debugName: 'h1',
      fontSize: headingH1SizeMobile,
    },
    screens.mediaMain({
      fontSize: headingH1SizeDesktop
    }),
  );

  export const h2 = style(
    {
      ...heading,
      $debugName: 'h2',
      fontSize: headingH2SizeMobile,
      fontWeight: typography.fontWeightBold,
    },
    screens.mediaMain({
      fontSize: headingH2SizeDesktop
    }),
  );

  export const h3 = style(
    {
      ...heading,
      $debugName: 'h3',
      fontSize: headingH3SizeDesktop,
      fontWeight: typography.fontWeightMedium,
    },
  );

  export const h4 = style(
    {
      ...heading,
      $debugName: 'h4',
      fontSize: headingH4SizeDesktop,
      fontWeight: typography.fontWeightMedium,
    },
  );

}

export default styles;

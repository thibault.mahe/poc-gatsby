import * as React from 'react';
import Link from 'gatsby-link';

interface ArticlesPageProps {
  data: {
    allContentfulArticle: {
      edges: [{
        node: {
          id: string,
          title: string,
          slug: string,
          author: {
            name: string
          },
          category: {
            name: string
          },
          excerpt: {
            childMarkdownRemark: {
              html: string
            }
          }
        }
      }]
    }
  };
}

const ArticlesPage = ({ data }: ArticlesPageProps) => (
  <div>
    <h1>Articles</h1>
    <ul>
      {data.allContentfulArticle.edges.map(edge => (
        <li key={edge.node.id}>
          <Link to={`/articles/${edge.node.slug}`}>
            <h3>{edge.node.title} - {edge.node.author.name}</h3>
          </Link>
          <p>{edge.node.category.name}</p>

          <p dangerouslySetInnerHTML={{
            __html: edge.node.excerpt.childMarkdownRemark.html
          }}/>

        </li>
      ))}
    </ul>
  </div>
);

export default ArticlesPage;

export const ArticlesPageQuery = graphql`
  query ArticlesPageQuery {
    allContentfulArticle(
      sort: {
        order: DESC,
        fields: [createdAt],
      }
    ) {
      edges {
        node {
          id
          title
          slug
          author {
            name
          }
          category {
            name
          }
          excerpt {
            childMarkdownRemark {
              html
            }
          }
        }
      }
    }
  }
`;

import * as React from 'react';
import { classes } from 'typestyle';
import Link from 'gatsby-link';
import { Grid, Row, Col } from 'react-flexbox-grid';

import { headings } from './../styles';
import Card from '../components/Card';


interface IndexPageProps {
  data: {
    allContentfulArticle: {
      edges: [{
        node: {
          id: string,
          title: string,
          slug: string,
          thumbnail: {
            file: {
              url: string,
              fileName: string,
              contentType: string,
            },
          },
          author: {
            name: string
          },
          category: {
            name: string
          },
          excerpt: {
            childMarkdownRemark: {
              html: string
            }
          }
        }
      }]
    },
    site: {
      siteMetadata: {
        title: string
      }
    }
  };
}

const IndexPage = ({ data }: IndexPageProps) => {
  return (
    <div>

      <h1 className={headings.default.h1}>{data.site.siteMetadata.title}</h1>

      <Grid>
        <Row>
          {data.allContentfulArticle.edges.map(edge => (
          <Col xs={12} sm={6} md={4} key={edge.node.id}>

            <Card
              contentLink = {`/articles/${edge.node.slug}`}
              contentIlluHref = {edge.node.thumbnail.file.url}
              contentTitle = {edge.node.title}
              contentExcerpt = {edge.node.excerpt.childMarkdownRemark.html}
              contentDate = '12'
              authorAvatarUrl = {edge.node.thumbnail.file.url}
              authorName = {edge.node.author.name}
            />

          </Col>
          ))}
        </Row>
      </Grid>

    </div>
  );
};

export default IndexPage;

export const IndexPageQuery = graphql`
  query IndexPageQuery {
    allContentfulArticle(
      sort: {
        order: DESC,
        fields: [createdAt],
      }
    ) {
      edges {
        node {
          id
          title
          slug
          thumbnail {
            file {
              url
              fileName
              contentType
            }
          }
          author {
            name
          }
          category {
            name
          }
          excerpt {
            childMarkdownRemark {
              html
            }
          }
        }
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
`;

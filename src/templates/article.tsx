import * as React from 'react';
import Link from 'gatsby-link';

interface ArticlePageProps {
  data: {
    contentfulArticle: {
      slug: string,
      title: string,
      author: {
        name: string
      },
      category: {
        name: string
      },
      body: {
        childMarkdownRemark: {
          html: string
        }
      }
    }
  };
}

const ArticlePage = ({ data }: ArticlePageProps) => {
  return (
    <div>
      <h1>{data.contentfulArticle.title}</h1>
      <h2>{data.contentfulArticle.author.name}</h2>
      <p>{data.contentfulArticle.category.name}</p>
      <p dangerouslySetInnerHTML={{
          __html: data.contentfulArticle.body.childMarkdownRemark.html
        }}/>
    </div>
  );
};

export default ArticlePage;

export const ArticlePageQuery = graphql`
  query ArticlePageQuery($slug: String!) {
    contentfulArticle(slug: { eq: $slug }) {
      title,
      slug,
      author {
        name
      },
      category {
        name
      },
      body {
        childMarkdownRemark {
          html
        }
      }
    }
  }
`;
